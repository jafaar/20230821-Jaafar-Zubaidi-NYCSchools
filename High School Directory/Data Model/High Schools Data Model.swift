//
//  High Schools Data Model.swift
//  High School Directory
//
//  Created by Jaafar Zubadi on 8/21/23.
//

import Foundation

struct highSchoolsDataModel: Decodable {
    let dbn: String?
    let school_name: String?
}

class satScoreDataModel: Decodable {
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}
