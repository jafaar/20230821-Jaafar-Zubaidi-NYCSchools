//
//  Network.swift
//  High School Directory
//
//  Created by Jaafar Zubadi on 8/21/23.
//

import Foundation
import UIKit


protocol sendData {
    func holdResult (data : [highSchoolsDataModel]? , error : Error?)
    func holdRecord (data : [satScoreDataModel]? , error : Error?)

}

class Network {
    
    static let shared = Network .init()
    var contaner : sendData?
    var schoolrecord = [satScoreDataModel]()
    
// function  makes request to connect to the web server
    func fetchData(){
       guard let url = URL.init(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        else {return}
        
        URLSession.shared.dataTask(with: url, completionHandler: {data , urlresponse , error in
            guard let data = data ,
            error == nil else {
                print("error : \(String(describing: error))")
                self.contaner?.holdResult(data: [], error: error)
                return
            }
   //decode the Data to the data Model
                do {
                    let objModel = try JSONDecoder().decode([highSchoolsDataModel].self, from: data)
                    self.contaner?.holdResult(data: objModel, error: error)
                }
                   catch{
                    print (error.localizedDescription)
                }
        }).resume()
    }
    
    func fetchSchoolData(){
       guard let url = URL.init(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        else {return}
        
        URLSession.shared.dataTask(with: url, completionHandler: {data , urlresponse , error in
            guard let data = data ,
            error == nil else {
                print("error : \(String(describing: error))")
                self.contaner?.holdRecord(data: [], error: error)
                return
            }
   //decode the Data to the data Model
                do {
                    let objModel = try JSONDecoder().decode([satScoreDataModel].self, from: data)
                    self.contaner?.holdRecord(data: objModel, error: error)

                }
                   catch{
                    print (error.localizedDescription)
                }
        }).resume()
    }
}
