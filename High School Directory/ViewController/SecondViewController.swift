//
//  SecondViewController.swift
//  High School Directory
//
//  Created by Jaafar Zubadi on 8/18/23.
//

import UIKit

class SecondViewController: UIViewController, sendData {
   
    //init(selectedSchool: highSchoolsDataModel)
    var school: highSchoolsDataModel?
    var records = [satScoreDataModel]()
    let labelRead = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let namelabel = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 70))
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let labelWrite = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let label2 = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let labelMath = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    let label3 = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 125, green: 192/255.0, blue: 203/255.0, alpha: 1.0)
        setUP()
        Network.shared.contaner = self
        //Called the function from Network Class
        Network.shared.fetchSchoolData()
    }
    func holdRecord(data: [satScoreDataModel]?, error: Error?) {
        guard let records = data else {
            fatalError("unable to find the information")
        }
        for recod in records {
            if recod.dbn == school?.dbn {
                DispatchQueue.main.async {
                    self.namelabel.text = recod.school_name
                    self.label.text = recod.sat_critical_reading_avg_score
                    self.label2.text = recod.sat_writing_avg_score
                    self.label3.text = recod.sat_math_avg_score

                }
            }
            
        }
    }
    
    func setUP() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back", style: .plain, target: self, action: #selector(backAction))
        navigationItem.leftBarButtonItem?.tintColor = .black
        
        namelabel.center = CGPoint(x: 250, y: 150)
        namelabel.numberOfLines = 0
        namelabel.textColor = .black
       
        self.view.addSubview(namelabel)
        
        label.center = CGPoint(x: 200, y: 200)
        label.textAlignment = .center
        label.textColor = .black
        
        self.view.addSubview(label)
        
        label2.center = CGPoint(x: 200, y: 250)
        label2.textAlignment = .center
        label2.textColor = .black
        
        self.view.addSubview(label2)
        
        label3.center = CGPoint(x: 200, y: 300)
        label3.textAlignment = .center
        label3.textColor = .black
        
        self.view.addSubview(label3)
        
        labelRead.center = CGPoint(x: 100, y: 200)
        labelRead.font = .boldSystemFont(ofSize: 16)
        labelRead.textAlignment = .center
        labelRead.textColor = .black
        labelRead.text = "READING : "
        self.view.addSubview(labelRead)
        
        labelWrite.center = CGPoint(x: 100, y: 250)
        labelWrite.font = .boldSystemFont(ofSize: 16)
        labelWrite.textAlignment = .center
        labelWrite.textColor = .black
        labelWrite.text = "WRITING : "
        self.view.addSubview(labelWrite)
        
        labelMath.center = CGPoint(x: 100, y: 300)
        labelMath.font = .boldSystemFont(ofSize: 16)
        labelMath.textAlignment = .center
        labelMath.textColor = .black
        labelMath.text = "MATH : "
        self.view.addSubview(labelMath)
    }
    
    @objc func backAction(){
        navigationController?.popToRootViewController(animated: true)
    }
    
    func holdResult(data: [highSchoolsDataModel]?, error: Error?) {
    }
    
    
}
