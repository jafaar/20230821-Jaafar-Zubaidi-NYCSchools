//
//  ViewController.swift
//  High School Directory
//
//  Created by Jaafar Zubadi on 8/21/23.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, sendData {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "High School Directory"
        self.SchoolsTable.backgroundColor = .white
        
        Network.shared.contaner = self
        //Called the function from Network Class
        Network.shared.fetchData()
        
    }
    
    @IBOutlet weak var SchoolsTable: UITableView!
    
    
    //create the object
    var schoolsName = [highSchoolsDataModel]()
    
    //function to update the table view in the main thread
    func holdResult(data: [highSchoolsDataModel]?, error: Error?) {
        if let schoolInfo = data{
            schoolsName = schoolInfo
        }
        DispatchQueue.main.async {
            self.SchoolsTable.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SchoolsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SchoolCellTableViewCell
        let schoolrecord = schoolsName[indexPath.row]
        cell.SchoolNameLabel.text = schoolrecord.school_name
        cell.SchoolNameLabel.textColor = .black        
        
        return cell
    }
    
    //function to display the second screen with details
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSchool = schoolsName[indexPath.row]
       let controller = SecondViewController()
        controller.school = selectedSchool
            navigationController?.pushViewController(controller, animated: true)
    }
    
    func holdRecord(data: [satScoreDataModel]?, error: Error?) {
    }
    
}



